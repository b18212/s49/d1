//* Fetch Method
//getting a request from API

//* Syntax for fetch
//? fetch('url', options)
// url -  this is the url which the request is to be made
// options - an array of properties, optional parameter

//! Show Posts
fetch("https://jsonplaceholder.typicode.com/posts")
    .then((response) => response.json())
    .then((data) => showPosts(data));
//.then - to get the response from the API

// show post function

const showPosts = (posts) => {
    let postEntries = ""; // empty string to store the posts

    posts.forEach((post) => {
        console.log(post); // to see the posts in the console

        // id is the post id
        postEntries += `
        <div id= "post-${post.id}">
            <h3 id="post-title-${post.id}">${post.title}</h3>
            <p id="post-body-${post.id}">${post.body}</p>
            <button onclick= "editPost('${post.id}')">Edit</button>
            <button onclick= "deletePost('${post.id}')">Delete</button>
        </div>

        `;
    });
    // to display the posts in the DOM
    document.querySelector("#div-post-entries").innerHTML = postEntries;
    // querySelector - to select the element in the DOM
};

//! Add Post
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
    // event listener to add the post
    e.preventDefault(); // to prevent the default behaviour of the form // to avoid to be submitted

    fetch("https://jsonplaceholder.typicode.com/posts", {
            // to add the post to the API)

            // to add the post to the API
            method: "POST",
            body: JSON.stringify({
                title: document.querySelector("#txt-title").value,
                body: document.querySelector("#txt-body").value,
                userId: 1,
            }),
            headers: {
                "Content-Type": "application/json",
            },
        })
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
            alert("Successfully added");

            // querySelector - to clear the form to make it black input field after adding or submitting the post
            document.querySelector("#txt-title").value = null;
            document.querySelector("#txt-body").value = null;
        });
});

//! Edit Post
const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML; // to get the title of the post
    let body = document.querySelector(`#post-body-${id}`).innerHTML; // to get the body of the post

    document.querySelector("#txt-edit-id").value = id; // to store the id of the post in the hidden input field
    document.querySelector("#txt-edit-title").value = title; // to store the title of the post in the hidden input field
    document.querySelector("#txt-edit-body").value = body; //  to store the body of the post in the hidden input field

    document.querySelector("#btn-submit-update").removeAttribute("disabled"); // to enable the button to update the post but it will not actually update the post.
    // removeAttribute - to remove the attribute from the element
};

//!Update Post
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
    e.preventDefault(); // to prevent the default behaviour of the form // to avoid to be submitted

    fetch("https://jsonplaceholder.typicode.com/posts/1", {
            method: "PUT",
            body: JSON.stringify({
                id: document.querySelector("#txt-edit-id").value,
                title: document.querySelector("#txt-edit-title").value,
                body: document.querySelector("#txt-edit-body").value,
                userId: 1,
            }),
            headers: {
                "Content-Type": "application/json",
            },
        })
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
            alert("Post Successfully updated");

            // querySelector - to clear the form to make it black input field after adding or submitting the post
            document.querySelector("#txt-edit-id").value = null;
            document.querySelector("#txt-edit-title").value = null;
            document.querySelector("#txt-edit-body").value = null;

            // to disable the button to update the post
            document
                .querySelector("#btn-submit-update")
                .setAttribute("disabled", true);
        });
});

//? Activity

/*
    >> Write the necessary code to delete a post
    >> Make a function called deletePost.
    >> Pass a parameter id.
    >> Use fetch method and the options

*/
//! Delete Post
const deletePost = (id) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
            },
        })
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
            alert("Post Successfully deleted");

            document.querySelector(`#post-${id}`).remove();
            console.log(id);
        });
};